class ObjectCreateError extends ErrorEvent {
  constructor(objectName, propName, message) {
    super(message);
    this.name = this.constructor.name;
    if (message) {
      this.message = message;
    } else {
      this.message = `Bad object data: ${propName}!`;
    }
    this.objectName = objectName;
  }
}

class MapObject {
  /**
   *
   * Create a new Map Object
   *
   * @param { Object } args
   */
  constructor({
    title,
    id,
    longitude,
    latitude,
    rate,
    type,
    phone,
    address,
    site,
    corporate,
  }) {
    this.id = this.formatNumber(id);
    this.title = title.trim();
    this.lngLat = this.formatLngLat(longitude, latitude);
    this.rate = rate
      ? { num: this.formatNumber(rate), format: this.formatRate(rate) }
      : {};
    this.type = type;
    this.phone = phone ? this.formatPhone(phone) : '';
    this.address = address;
    this.site = site ? this.formatUrl(site) : '';
    this.corporate = corporate;
  }

  /**
   *
   * Parse val to number
   *
   * @param { String || Number } num
   */
  formatNumber(num) {
    if (typeof num === 'string') {
      return num.length > 0
        ? Number(num.replace(/\s/g, '').replace(',', '.'))
        : 0;
    }
    return num;
  }

  /**
   *
   * @param lng
   * @param lat
   */
  formatLngLat(lng, lat) {
    return [this.formatNumber(lng), this.formatNumber(lat)];
  }

  formatDistance(val) {
    return this.formatNumber(val) + ' км.';
  }

  formatRate(n) {
    const num = Math.round(this.formatNumber(n)) + '';
    return num.replace(/\B(?=(\d{3})+(?!\d))/g, ' ') + 'тыс.т/год';
  }

  formatPhone(val) {
    const format = (phone) => {
      const cleaned = ('' + phone).replace(/\D/g, '').trim();
      if (cleaned.length <= 12) {
        const prepared = '380'.slice(0, 12 - cleaned.length) + cleaned;
        if (prepared.length === 12) {
          const match = prepared.match(/(380|)?(\d{2})(\d{3})(\d{2})(\d{2})$/);
          if (match) {
            return `+${match[1]} (${match[2]}) ${match[3]} ${match[4]} ${match[5]}`;
          }
        }
      }
      return phone;
    };
    return (Array.isArray(val) ? val : val.slice(',')).map(format);
  }

  formatUrl(url) {
    return new URL(url).hostname;
  }

  static createObject(data) {
    if (data.id === undefined && data.id === null) {
      throw new ObjectCreateError(data.title, 'id');
    }
    if (
      data.longitude === undefined &&
      data.longitude === null &&
      data.longitude > 0
    ) {
      throw new ObjectCreateError(data.title, 'longitude');
    }
    if (
      data.latitude === undefined &&
      data.latitude === null &&
      data.latitude > 0
    ) {
      throw new ObjectCreateError(data.title, 'latitude');
    }
    return new this(data);
  }
}

class Consumer extends MapObject {
  constructor({ kKal, ...args }) {
    super(args);
    this.kKal = kKal;
  }
}

class Mine extends MapObject {
  constructor({ ash, ...args }) {
    super(args);
    this.ash = ash;
  }
}

const getCurrentObject = (obj = {}) => {
  return new Proxy(obj, {
    get: (target, name) => {
      const error = { error: 'Информация недоступна' };
      if (!(name in target)) {
        return error;
      }
      if (target[name] !== 0 && (!target[name] || target[name].length <= 3)) {
        return error;
      }
      return target[name];
    },
  });
};

export { MapObject, Consumer, Mine, getCurrentObject };
