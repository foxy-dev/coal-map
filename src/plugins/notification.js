import toast from '@/utils/notify';

export default {
  install(Vue, options) {
    Vue.prototype.$notify = {
      error(params) {
        toast(Object.assign(params, { type: 'danger' }));
      },
      warning(params) {
        toast(Object.assign(params, { type: 'warning' }));
      },
      success(params) {
        toast(Object.assign(params, { type: 'success' }));
      },
    };
  },
};
