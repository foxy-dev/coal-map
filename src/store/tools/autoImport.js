const modulesFiles = require.context('@/store/modules', false, /\.js$/);
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  const moduleName = modulePath
    .replace(/^\.\/(.*)\.\w+$/, '$1')
    .replace(/-([a-z])/g, (g) => g[1].toUpperCase());
  const value = modulesFiles(modulePath);
  modules[moduleName.toUpperCase()] = value.default;
  return modules;
}, {});

export default modules;
