import Resource from '@/api/utils/Resource';
const apiResource = new Resource('static');

const state = {
  corporations: null,
  groupTypes: null,
  objectTypes: null,
  loading: false,
  status: false,
};
const getters = {
  getCorporations: (state) => state.corporations,
  getCorporate: (state) => (id) =>
    Object.keys(state.corporations).includes(id) && state.corporations[id],
};
const mutations = {
  LOAD_STATIC_DATA: (state) => {
    state.loading = true;
  },
  ERROR_STATIC_DATA: (state) => {
    state.loading = false;
    state.status = false;
  },
  SUCCESS_STATIC_DATA: (state, data) => {
    Object.assign(state, data);
    state.loading = false;
    state.status = true;
  },
};

const actions = {
  async getStaticTypes({ commit }, query = '') {
    commit('LOAD_STATIC_DATA', query, { root: true });
    const data = await apiResource.list(query);
    commit(`${data ? 'SUCCESS' : 'ERROR'}_STATIC_DATA`, data);
    return data;
  },
};
export async function getStaticData({ commit }, query = '') {
  const data = await apiResource.list(query);
  commit(`${data ? 'SUCCESS' : 'ERROR'}_STATIC_DATA`, data);
}
export default {
  namespaced: false,
  state,
  getters,
  mutations,
  actions,
};
