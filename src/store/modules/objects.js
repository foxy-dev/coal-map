import { Consumer, getCurrentObject } from '@/api/models/MapObject';
import Resource from '@/api/utils/Resource';
const apiResource = new Resource('objects');

const state = {
  objects: null,
  filter: null,
  loading: false,
  status: false,
};

const getters = {
  getObjectsList: (state) => state.objects,
  getFilteredObjectsList: (state) => (type = undefined) =>
    state.objects.filter((obj) => (type ? obj.type === type : obj.type)),
  getObjectById: (state) => (id) => {
    return getCurrentObject(state.objects.find((obj) => obj.id === id));
  },
  getFeaturesCollection: (state) => state.objects,
};

const mutations = {
  LOAD_OBJECTS: (state) => {
    state.loading = true;
  },
  ERROR_OBJECTS: (state) => {
    state.loading = false;
    state.status = false;
  },
  SUCCESS_OBJECTS: (state, data) => {
    state.objects = data;
    state.loading = false;
    state.status = true;
  },
};

const actions = {
  async getObjects({ commit }, query = '') {
    commit('LOAD_OBJECTS', query, { root: true });
    try {
      const response = await apiResource.list(query);
      commit(
        'SUCCESS_OBJECTS',
        response.data.map((data) => Consumer.createObject(data))
      );
    } catch (e) {
      commit(`ERROR_OBJECTS`);
      throw new Error(e);
    }
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
