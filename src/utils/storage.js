class Storage {
  constructor(key) {
    this.key = key;
  }
  get() {
    localStorage.getItem(this.key);
  }
  set(value) {
    localStorage.setItem(this.key, value);
  }
  remove() {
    localStorage.removeItem(this.key);
  }
}

export { Storage as default };
