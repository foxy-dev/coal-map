import iziToast from 'izitoast';
import 'izitoast/dist/css/iziToast.min.css';

export default ({ type, ...params }) => {
  iziToast[type]({ cb: 5000, position: 'topRight', ...params });
};
