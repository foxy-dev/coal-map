// Utilities
import { storyFactory } from '../util/helpers'
import { text, boolean } from '@storybook/addon-knobs'

export default { title: 'Vue Mapbox Map' }

function genComponent (name) {
  return {
    name,

    render (h) {
      return h('div', this.$slots.default)
    },
  }
}

const story = storyFactory({
    MglScaleControl: genComponent('MglScaleControl'),
    MglMap: genComponent('MglMap'),
    Mapbox: genComponent('Mapbox'),
})

export const asDefault = () => story({
  props: {
    actions: {
      default: boolean('Actions', false),
    },
    cardText: {
      default: text('Card text', 'Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Donec sodales sagittis magna. Vestibulum dapibus nunc ac augue. Donec sodales sagittis magna. Duis vel nibh at velit scelerisque suscipit.'),
    },
    divider: {
      default: boolean('Divider', false),
    },
    text: {
      default: boolean('Text', true),
    },
    title: {
      default: boolean('Show title', true),
    },
    titleText: {
      default: text('Title text', 'Card title'),
    },
  },
  template: `
    <MglMap
        class="map-wrapper"
        :access-token="accessToken"
        :map-style.sync="mapStyle"
        :center="defaultPosition.center"
        :zoom="defaultPosition.zoom"
        :pitch="0"
        :min-zoom="defaultPosition.zoom - 0.1"
        :max-zoom="18"
        @zoom="zoomEnd"
        @click="handleClick"
        @load="onLoad"
        >
        <MglScaleControl position="bottom-left" />
        <slot name="markers" />
    </MglMap>
  `,
})
